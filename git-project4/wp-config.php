<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'git-project4' );

/** MySQL database username */
define( 'DB_USER', 'git-project4' );

/** MySQL database password */
define( 'DB_PASSWORD', 'git-project4' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't-i:<s$.;}sf^&~X$-w/5(o4kI,2#W=qkNT?v1LJ?Q..I/%E0-V+nsG}.J+Bt_w{' );
define( 'SECURE_AUTH_KEY',  'LQs#MFG{mhAqr7Ov6Rg% TS)sI0X+(LR$lueB05cFa/<WZ~?v ?Y%{s<NF]7AU}o' );
define( 'LOGGED_IN_KEY',    'kg!AqT<HTWu-9{JBEczUgli<<0QC/]M[;zRh|d/xZ{:?vs1?#0i3u.@STFw<.p|d' );
define( 'NONCE_KEY',        'F4s4(Sef,<&C`]Q5<>B<Z?Tx^hSaN4>e?lBA84b) XqS-q]H!&.Akg~NFY;RLS3v' );
define( 'AUTH_SALT',        '%[r6,~v +D3+Ym8.$-G=nAN;Yxy2]8p<3#v-DwBfZ&?#Oa8r~7o<yrZ6kWB5}/!^' );
define( 'SECURE_AUTH_SALT', 'I-QOIHx1W>%_R!cFh~+)Vzw<6yqSF14)bX5D_V@<Zy,3N>)`RQ,VWEAnE4UjQkdT' );
define( 'LOGGED_IN_SALT',   'w/-,qk?)^4nhE6t*Z&ZsEm7,!{<@=JDb;oZb#$t5HXDZ*qj+Pn!Y[;nfaXpIH(O)' );
define( 'NONCE_SALT',       ')0>8ZaPv8e;93CS#)_FPTRg$47Tg;92OL<zC;oj{jiT?=_`Wd8jeprieBGxf-4Af' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
